FROM mcr.microsoft.com/dotnet/runtime:3.1
RUN mkdir -p /app
COPY bin/Debug/netcoreapp3.1 ./app
WORKDIR /app
ENTRYPOINT ["dotnet","HelloWord.dll"]